

## About Nomina

program web ini digunakan untuk mengikuti test masuk di nomina.didalam web terdapat 2 role user yaitu superadmin dan member

## role superadmin
role superadmin dapat mengelola :
- data member, 
- data hobby, 
- dan data user superadmin. 

user akses : 
	email : superadmin@domain.com
	password : 12345678

## role member
role member dapat mengelola :
-  data member sesuai dengan user login

user akses : 
	email : member@domain.com
	password : 12345678

## instalasi laravel : 
- clone repository atau unzip folder kedalam local komputer / htdoct /
- copy .env.example ke .env
- buat database baru, kemudian setting file .env bagian database sesuai settingan database anda:
	`DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nomina
DB_USERNAME=root
DB_PASSWORD=password`

- jalankan perintah command line berikut :
	- composer update
	- php artisan migrate --seed
	- npm install && npm run dev (opsional)
- jalankan web dengan cara :
	- php artisan serve

## penggunaan api

untuk menjalankan api anda bisa menggunakan postman. 
import postman dari url berikut : https://www.getpostman.com/collections/0b289c6a458ee267aef1 
