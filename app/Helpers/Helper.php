<?php

use Illuminate\Support\Facades\App;

if(!function_exists('ListPerPage')){
    function ListPerPage()
    {
        return ['10' => '10', '25' => '25', '50' => '50', '100' => '100', '500' => '500', '1000' => '1000'];
    }
}

if(!function_exists('status_aktif')){
    function status_aktif()
    {
        return array(
            1 => 'Aktif',
            2 => 'Non Aktif',
        );
    }
}


if(!function_exists('status_aktif_color')){
    function status_aktif_color()
    {
        return array(
            1 => 'success',
            2 => 'danger',
        );
    }
}

if(!function_exists('formating_number')){
    function formating_number($number)
    {        
        return number_format($number,0,',','.');
    }
}