<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\ApiController;

use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;

//use Illuminate\Support\Str;
use Auth;
use Validator;

use App\Models\User;

class AuthController extends ApiController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'password_confirmation' => 'same:password',
            'age' => 'required|string|min:0|max:100'
        ]);

        if($validator->fails()){
            return $this->errorResponse('error validasi',$validator->messages(), 422);     
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'age' => $request->age,
            'password' => Hash::make($request->password)
        ]);

        $user->syncRoles('member');

        $token = $user->createToken('auth_token')->plainTextToken;

        $hasil = array(
            'token' => $token,
            'token_type' => 'Bearer',
            'uuid' => $user->uuid,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'age' => $user->age,
            'role' => $user->getRoleNames()[0],
            
        );
        return $this->successResponse($hasil,'success register', 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8'
        ]);

        if($validator->fails()){
            return $this->errorResponse('error validasi',$validator->messages(), 422);     
        }

        if (!Auth::attempt($request->only('email', 'password')))
        {
            return $this->errorResponse('email password salah',$validator->messages(), 422);  
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        $hasil = array(
            'token' => $token,
            'token_type' => 'Bearer',
            'uuid' => $user->uuid,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'age' => $user->age,
            'role' => $user->getRoleNames()[0],
        );
        return $this->successResponse($hasil,'success login', 200);
    }

    // method for user logout and delete token
    public function logout()
    {
        auth()->user()->tokens()->delete();
        $hasil = array();
        return $this->successResponse($hasil,'You have successfully logged out and the token was successfully deleted', 200);
        
    }

}