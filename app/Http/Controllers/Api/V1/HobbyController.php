<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;

use Validator;
use Session;
use Auth;

use App\Models\Hobby;

class HobbyController extends ApiController
{
    

    public function index()
    {
        $data_all = Hobby::get();
        $result = array();
        foreach ($data_all as $item) {
            $result[] = array(
                'uuid' => $item->uuid,
                'nama' => $item->nama,
            );
        }
        return $this->successResponse($result,'success', 200);

    }

    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'aktif' => 'required',
        ]);

        if ($valid->fails()) {
            return $this->errorResponse('error validasi',$valid->messages(), 422);
        } else {
            $data = new Hobby();
            $data->nama = $request->nama;
            $data->aktif = $request->aktif;

            $data->save();    
            return $this->successResponse('success','success save hobby', 200);
        }

            
        
    }

    public function update(Request $request,$uuid)
    {
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'aktif' => 'required',
        ]);

        if ($valid->fails()) {
            return $this->errorResponse('error validasi',$valid->messages(), 422);
        } else {
            $data = Hobby::where('uuid',$uuid)->first();
            if (empty($data)) {
                return $this->errorResponse('error validasi','uuid tidak ditemukan', 422);
            }
            
            $data->nama = $request->nama;
            $data->aktif = $request->aktif;

            $data->update();    
            return $this->successResponse('success','success update hobby', 200);
        }
    }

    public function destroy($uuid)
    {
        Hobby::where('uuid',$uuid)->delete();
        return $this->successResponse('success Delete', 200);
    }
}