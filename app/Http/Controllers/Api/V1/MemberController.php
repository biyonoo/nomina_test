<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;

use Validator;
use Session;
use Auth;

use App\Models\Hobby;
use App\Models\User;
use App\Models\UserHobby;

class MemberController extends ApiController
{
    

    public function index(Request $request)
    {
        $per_page = !empty($request->per_page) ? $request->per_page : 10;
        $search = $request->search;

        $query = User::query();
        if ($search != '') {
            $query->where('first_name','like','%'.$search.'%');
            $query->orwhere('last_name','like','%'.$search.'%');
        }

        $data_all = $query->role('member')->latest()->paginate($per_page);;
        $hasil = array();
        foreach ($data_all as $item) {
            $hasil[] = array(
                'uuid' => $item->uuid,
                'first_name' => $item->first_name,
                'last_name' => $item->last_name,
                'email' => $item->email,
                'age' => $item->age,
                'hobby' => $item->hobby->count(),
            );
        }

        $result = array(
            'item' => $data_all->count(),
            'perPage' => $data_all->perPage(),            
            'currentPage' => $data_all->currentPage(),
            'total' => $data_all->total(),
            'data' => $hasil
        );
        
        return $this->successResponse($result,'success', 200);

    }

    public function show($uuid)
    {
        $user = User::where('uuid',$uuid)->first();
        $data_hobby = UserHobby::where('id_user',$user->id)->get();
        $hobby = array();
        foreach($data_hobby as $item){
            $hobby[] = array(
                'uuid_hobby' => @$item->hobby->uuid,
                'nama' => @$item->hobby->nama,
            );
        }
        $result = array(
            'uuid' => $user->uuid,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'age' => $user->age,
            'hobby' => $hobby,
        );
        return $this->successResponse($result,'success', 200);
    }

    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required|min:0|max:100',            
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        if ($valid->fails()) {
            return $this->errorResponse('error validasi',$valid->messages(), 422);
        } else {
            $data = new User();
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;
            $data->password = bcrypt($request->password);

            if ($data->save()) {
                $data->assignRole('member');
                foreach($request->hobby as $item => $hasil){
                    $cek_hobby = Hobby::where('uuid',$hasil)->first();
                    if (!empty($cek_hobby)) {
                        $cek = new UserHobby();
                        $cek->id_user = $data->id;
                        $cek->id_hobbies = $cek_hobby->id;
                        $cek->save();
                    }
                        
                } 
            }

            return $this->successResponse('success','success add member', 200);
        }
            
        
    }

    public function update(Request $request,$uuid)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required|min:0|max:100',            
            'email' => 'required|email',
        ]);

        if ($valid->fails()) {
            return $this->errorResponse('error validasi',$valid->messages(), 422);
        } else {
            $data = User::where('uuid',$uuid)->first();
            if (empty($data)) {
                return $this->errorResponse('data tidak ditemukan',"", 422);
            }

            $cek_email = User::where('email',$request->email)->where('id','!=',$data->id)->first();
            if (!empty($cek_email)) {
                return $this->errorResponse('error validasi',["email"=>"email sudah digunakan"], 422);
            }
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;

            if ($data->save()) {
                $data->assignRole('member');
                UserHobby::where('id_user',$data->id)->delete();
                foreach($request->hobby as $item => $hasil){
                    $cek_hobby = Hobby::where('uuid',$hasil)->first();
                    if (!empty($cek_hobby)) {
                        $cek = new UserHobby();
                        $cek->id_user = $data->id;
                        $cek->id_hobbies = $cek_hobby->id;
                        $cek->save();
                    }
                        
                } 
            }

            return $this->successResponse('success','success add member', 200);
        }
    }

    public function destroy($uuid)
    {
        User::where('uuid',$uuid)->delete();
        return $this->successResponse('success Delete', 200);
    }
}