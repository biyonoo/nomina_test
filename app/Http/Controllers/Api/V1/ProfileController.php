<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\ApiController;
use Auth;
use Validator;

use App\Models\User;
use App\Models\UserHobby;
use App\Models\Hobby;

class ProfileController extends ApiController
{
    
    
    public function index()
    {
        $user = Auth::user();
        $data_hobby = UserHobby::where('id_user',$user->id)->get();
        $hobby = array();
        foreach($data_hobby as $item){
            $hobby[] = array(
                'uuid_hobby' => @$item->hobby->uuid,
                'nama' => @$item->hobby->nama,
            );
        }
        $hasil = array(
            'uuid' => $user->uuid,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'age' => $user->age,
            'role' => $user->getRoleNames()[0],
            'hobby' => $hobby,

        );
        return $this->successResponse($hasil,'success', 200);
    }

    public function profile_update(Request $request)
    {
        $valid = Validator::make($request->all(), [
            
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',            
            'email' => 'required|email',
        ]);

        if ($valid->fails()) {
            return $this->errorResponse('error validasi',$valid->messages(), 422);
        } else {
            $data = Auth::user();
            
            
            $cek_email = User::where('email',$request->email)->where('id','!=',$data->id)->first();
            if (!empty($cek_email)) {
                return $this->errorResponse('error validasi',"Email sudah ada", 422);
            }

            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;
            $data->save();

            
            return $this->successResponse($data,'success update profile', 200);
        }

    }

    public function update_password(Request $request)
    {
        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            // The passwords matches
            return $this->errorResponse('error validasi',"Your current password does not matches with the password.", 422);
        }

        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0){
            // Current password and new password same
            return $this->errorResponse('error validasi',"New Password cannot be same as your current password.", 422);
        }

        $validatedData = Validator::make($request->all(),[
            'old_password' => 'required',
            'new_password' => 'required|string|min:8',
            'confirm_password' => 'required|same:new_password',
        ]);
        if($validatedData->fails()){
            return $this->errorResponse('error validasi',$validatedData->messages(), 422);     
        }

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return $this->successResponse('success update password', 200);
    
    }
}