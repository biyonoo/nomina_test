<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;

use Validator;
use Session;
use Auth;

use App\Models\Hobby;
class HobbyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perpage = !empty($request->perpage) ? $request->perpage : 10;
        $search = $request->search;

        $query = Hobby::query();
        
        if (!empty($search)) {
            $query->where('nama', 'like', '%'.$search.'%');
        } 

        

        $data_all = $query->latest()->paginate(!empty($perpage) ? $perpage : 10);
        
        return view('data.hobby.index', compact('data_all','perpage','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data.hobby.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'nama' => 'required',
            'aktif' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('warning', 'data gagal di simpan');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {
            $data = new Hobby();
            $data->nama = $request->nama;
            $data->aktif = $request->aktif;

            if ($data->save()) {
                Session::flash('success', 'data berhasil di simpan');
            }
            
            return redirect('data/hobby');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $data = Hobby::where(['uuid' => $uuid])->first();
        if (empty($data)) {
            Session::flash('warning', 'data tidak ditemukan');
            return redirect('data/hobby');
        }
        return view('data.hobby.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $data = Hobby::where(['uuid' => $uuid])->first();
        if (empty($data)) {
            Session::flash('warning', 'data tidak ditemukan');
            return redirect('data/hobby');
        }

        
        return view('data.hobby.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $valid = Validator::make($request->all(), [            
            'nama' => 'required',
            'aktif' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('warning', 'data gagal di simpan');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {
            $data = Hobby::where('uuid',$uuid)->first();
            if (empty($data)) {
                Session::flash('warning', 'data tidak ditemukan');
                return redirect()->back()->withInput();
            }
            
            $data->nama = $request->nama;
            $data->aktif = $request->aktif;
            if ($data->save()) {
                Session::flash('success', 'data berhasil di simpan');
            }

            
            return redirect('data/hobby');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $data = Hobby::where(['uuid' => $uuid])->first();
        if (!empty($data)) {
            $data->delete();
            Session::flash('success', 'Delete Success');
        
            
        }else{
            Session::flash('warning', 'data tidak ditemukan');
        }   
        return redirect()->back();
    }

}