<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Validator;
use Session;
use Auth;

use App\Models\User;
use App\Models\Hobby;
use App\Models\UserHobby;
class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perpage = !empty($request->perpage) ? $request->perpage : 10;
        $search = $request->search;

        $query = User::query();
        
        if (!empty($search)) {
            $query->where('first_name', 'like', '%'.$search.'%');
            $query->orwhere('last_name', 'like', '%'.$search.'%');
        } 

        

        $data_all = $query->role('member')->with('hobby')->latest()->paginate(!empty($perpage) ? $perpage : 10);
        
        return view('data.member.index', compact('data_all','perpage','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_hobby = Hobby::where('aktif',1)->get();
        return view('data.member.create',compact('data_hobby'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required|min:0|max:100',            
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        if ($valid->fails()) {
            Session::flash('warning', 'data gagal di simpan');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {
            $data = new User();
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;
            $data->password = bcrypt($request->password);

            if ($data->save()) {
                $data->assignRole('member');
                foreach($request->hobby as $item => $hasil){
                    $cek = new UserHobby();
                    $cek->id_user = $data->id;
                    $cek->id_hobbies = $hasil;
                    $cek->save();
                } 
            }

            Session::flash('success', 'data berhasil di simpan');
            return redirect('data/member');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $data = User::where(['uuid' => $uuid])->first();
        if (empty($data)) {
            Session::flash('warning', 'data tidak ditemukan');
            return redirect('data/member');
        }
        return view('data.member.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $data = User::where(['uuid' => $uuid])->first();
        if (empty($data)) {
            Session::flash('warning', 'data tidak ditemukan');
            return redirect('data/member');
        }
        $selectedhobby = UserHobby::where('id_user',$data->id)->pluck('id_hobbies')->toArray();;
        $data_hobby = Hobby::where('aktif',1)->get();
        return view('data.member.edit', compact('data','data_hobby','selectedhobby'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $valid = Validator::make($request->all(), [
            
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',            
            'email' => 'required|email',
        ]);

        if ($valid->fails()) {
            Session::flash('warning', 'data gagal di simpan');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {
            $data = User::where('uuid',$uuid)->first();
            if (empty($data)) {
                Session::flash('warning', 'data tidak ditemukan');
                return redirect()->back()->withInput();
                }
            
            $cek_email = User::where('email',$request->email)->where('id','!=',$data->id)->first();
            if (!empty($cek_email)) {
                Session::flash('warning', 'email sudah terpakai');
                return redirect()->back();
            }

            
            if(!empty($request->password)){
                $data->password = bcrypt($request->password);
            }
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;
            if ($data->save()) {
                $data->assignRole('member');
                UserHobby::where('id_user',$data->id)->delete();
                foreach($request->hobby as $item => $hasil){
                    $cek = new UserHobby();
                    $cek->id_user = $data->id;
                    $cek->id_hobbies = $hasil;
                    $cek->save();
                } 
            }

            Session::flash('success', 'data berhasil di simpan');
            return redirect('data/member');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $data = User::where(['uuid' => $uuid])->first();
        if (!empty($data)) {
            $data->delete();
            Session::flash('success', 'Delete Success');
        
            
        }else{
            Session::flash('warning', 'data tidak ditemukan');
        }   
        return redirect()->back();
    }

}