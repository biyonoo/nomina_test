<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

use Validator;
use Session;
use Auth;

use App\Models\User;
use App\Models\Hobby;
use App\Models\UserHobby;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Auth::user();
        $selectedhobby = UserHobby::where('id_user',$data->id)->pluck('id_hobbies')->toArray();;
        $data_hobby = Hobby::where('aktif',1)->get();
        return view('home', compact('data','data_hobby','selectedhobby'));
    }

    public function profile()
    {
        $data = Auth::user();
        return view('profile',compact('data'));
    }

    public function profileUpdate(Request $request)
    {
        $valid = Validator::make($request->all(), [
            
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',            
            'email' => 'required|email',
        ]);

        if ($valid->fails()) {
            Session::flash('warning', 'data gagal di simpan');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {
            $data = Auth::user();
            
            
            $cek_email = User::where('email',$request->email)->where('id','!=',$data->id)->first();
            if (!empty($cek_email)) {
                Session::flash('warning', 'email sudah terpakai');
                return redirect()->back();
            }

            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;
            if ($data->save()) {
                Session::flash('success', 'data berhasil di update');
            }

            
            return redirect()->back();
        }
    
    }

    public function passwordUpdate(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        Session::flash('success', 'Password berhasil di update');
        return redirect()->back();
    }


    public function updateMember(Request $request)
    {
        $valid = Validator::make($request->all(), [
            
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',            
            'email' => 'required|email',
        ]);

        if ($valid->fails()) {
            Session::flash('warning', 'data gagal di simpan');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {
            $data = Auth::user();
            if (empty($data)) {
                Session::flash('warning', 'data tidak ditemukan');
                return redirect()->back()->withInput();
                }
            
            $cek_email = User::where('email',$request->email)->where('id','!=',$data->id)->first();
            if (!empty($cek_email)) {
                Session::flash('warning', 'email sudah terpakai');
                return redirect()->back();
            }

            
            if(!empty($request->password)){
                $data->password = bcrypt($request->password);
            }
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->email = $request->email;
            $data->age = $request->age;
            if ($data->save()) {
                UserHobby::where('id_user',$data->id)->delete();
                foreach($request->hobby as $item => $hasil){
                    $cek = new UserHobby();
                    $cek->id_user = $data->id;
                    $cek->id_hobbies = $hasil;
                    $cek->save();
                } 
            }

            Session::flash('success', 'data berhasil di simpan');
            return redirect()->back();
        }
    }
}
