<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class Hobby extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'nama',
        'aktif',
    ];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    } 

    public function user()
    {
        return $this->hasMany('App\Models\UserHobby', 'id_hobbies', 'id');
    }

    public function status_text(){
        $aktif = status_aktif();
        return isset($aktif[$this->aktif])?$aktif[$this->aktif]:'';
    }

    public function status_color(){
        $aktif = status_aktif_color();
        return isset($aktif[$this->aktif])?$aktif[$this->aktif]:'';
    }
}
