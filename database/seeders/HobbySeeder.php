<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class HobbySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('id_ID');

        $data_hobby = array(
            1=>'Sepak Bola',
            2=>'Bulu tangkis',
            3=>'Menyanyi',
            4=>'Menggambar',
            5=>'Memancing',
            6=>'Bersepeda',
            7=>'Berenang',
            8=>'Main Game',
            9=>'Voly',
            10=>'Basket',
        );
        foreach($data_hobby as $row=> $item){
            $data = new \App\Models\Hobby();
            $data->nama  = $item;
            $data->aktif = $faker->numberBetween(1,2);;
            $data->save();
        }
    }
}
