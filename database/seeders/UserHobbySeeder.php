<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Models\User;
use App\Models\Hobby;
use App\Models\UserHobby;
class UserHobbySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('id_ID');
        for ($i=1; $i <= 500; $i++) {
            $user = User::where('id','!=','1')->inRandomOrder()->first();
            $hobby = Hobby::where('aktif',1)->inRandomOrder()->first();
            $data = UserHobby::firstOrNew([
                'id_user' => $user->id,
                'id_hobbies' => $hobby->id,
            ]);
            $data->save();
        }
    }
}
