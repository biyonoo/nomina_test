<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

use Faker\Factory as Faker;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Role::create(['name' => 'superadmin']);
        Role::create(['name' => 'member']);

        $faker = Faker::create('id_ID');

        $super = new User();
        $super->first_name = 'superadmin';
        $super->last_name = 'superadmin';
        $super->email = 'superadmin@domain.com';
        $super->password = bcrypt('12345678');
        $super->age = 30;
        $super->email_verified_at = \Carbon\Carbon::now();
        $super->save();
        $super->assignRole('superadmin');

        $data_member = new User();
        $data_member->first_name = 'member';
        $data_member->last_name = 'user';
        $data_member->email = 'member@domain.com';
        $data_member->password = bcrypt('12345678');
        $data_member->age = 30;
        $data_member->email_verified_at = \Carbon\Carbon::now();
        $data_member->save();
        $data_member->assignRole('member');        

        for ($i=1; $i <= 100; $i++) { 
            $email = $faker->email;
            $member = User::firstOrNew(['email' =>  $email ]);
            $member->first_name = $faker->firstName();
            $member->last_name = $faker->lastName;
            $member->email = $email;
            $member->password = bcrypt('12345678');
            $member->age = $faker->numberBetween(10,80);
            $member->email_verified_at = \Carbon\Carbon::now();
            $member->save();
            $member->assignRole('member');
        }
    }
}
