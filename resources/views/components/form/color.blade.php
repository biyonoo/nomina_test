@props([
    'label' => false,
    'for'=> false,
    'error' => false
])
<div class="form-group mb-3">
    <label class="text-capitalize" for="{{ $for }}">{{ $label }}</label>
    <input type="color"  class="form-control form-control-sm {{ $error ? 'is-invalid':'' }}  " id="{{ $for }}" {{ $attributes }}>
    @if($error)
        <span class="invalid-feedback text-capitalize" role="alert">{{ $error }}</span>
    @endif
</div>