@props([
    'class' => false,
    'name' =>false,
    'label' => false,
    'for'=> false,
    'data' => false,
    'value' => false,
    'error' => false
])


<div class="form-group mb-3">
    <label class="text-capitalize" for="{{ $for }}">{{ $label }}</label><br>
    {{ Form::select($name,[''=>'Pilih Data']+$data?$data:'', $value,['class'=>'form-select form-select-sm select2','id'=>$for]) }}
    @if($error)
        <span class="invalid-feedback text-capitalize" role="alert">{{ $error }}</span>
    @endif
</div>