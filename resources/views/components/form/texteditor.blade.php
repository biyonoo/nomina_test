@props([
    'label' => false,
    'for'=> false,
    'error' => false,
    'value'=>false
])
<div class="form-group mb-3">
    <label for="{{ $for }}">{{ $label }}</label>
    <textarea  class="form-control texteditor {{ $error ? 'is-invalid':'' }}" id="{{ $for }}" {{ $attributes }}>
        {{ $value }}
    </textarea>
    @if($error)
        <span class="invalid-feedback text-capitalize" role="alert">{{ $error }}</span>
    @endif
</div>