@extends('layouts.app')

@section('content')
    
    <div class="container">
        <form method="POST" action="{{ url('data/hobby',$data->uuid) }}" enctype="multipart/form-data">
            @csrf
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-sm-center">
                            <div>Data hobby</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <x-form.text label="nama" for="nama" name="nama" value="{{ $data->nama }}" :error="$errors->first('nama')" ></x-form.text>  
                                </div>
                                <div class="col-md-6">
                                    <div class="col-12 ">
                                        <x-form.select label="aktif" for="aktif" name="aktif"  :data="[''=>'pilih']+status_aktif()" :value="$data->aktif" :error="$errors->first('aktif')"></x-form.select>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary "><i class="fa fa-save me-2"></i>Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
                    
        </form>
    </div>
@endsection
