@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header  d-flex justify-content-between align-items-sm-center align-items-sm-center">
                <div class="text-capitalize">user hobby</div>
                <div class="">
                    <a href="{{ url('data/hobby/create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus me-1"></i>Tambah Data</a>
                </div>
            </div>
            <div class="card-body pb-0 border border-bottom-1">
                <form class="mb-0">
                    <div class="row">
                        
                        <div class="col">
                            <x-form.text label="Cari" for="search" name="search" value="{{ $search }}" :error="$errors->first('search')" ></x-form.text>  
                        </div>
                        
                        <div class="col">
                            <label>Tampil perpage</label>
                            {{ Form::select('perpage', ListPerPage(), $perpage,['class'=>'form-select form-select-sm']) }}
                        </div>
                        <div class="col">
                            <label>&nbsp</label>
                            <button type="submit" class="btn btn-primary btn-sm w-100 " name="submit" value="view"><i class="fa fa-filter me-1"></i> Filter</button>
                        </div>

                    </div>

                    

                </form>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>no</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data_all as $item)
                            <tr>
                                <td>{{ $data_all->firstItem()+$loop->index }}</td>
                                <td>{{ $item->nama }}</td>
                                <td><span class="badge bg-{{ $item->status_color() }}">{{ $item->status_text() }}</span></td>
                                <td class="white-space">
                                    
                                    <a class="btn btn-warning btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"  href="{{ url('data/hobby/'.$item->uuid.'/edit') }}"><i class="fas fa-edit me-1"></i> Edit</a>
                                    
                                    <div class="btn btn-danger btn-sm"  data-bs-toggle="tooltip" data-bs-placement="top" title="Hapus"  onclick="return deleteAlert('{{url('data/hobby/delete/'.$item->uuid)}}')" ><i class="fas fa-trash me-1"></i> Hapus</div>
                                    
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-danger">Belum ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="d-flex align-items-center justify-content-between">
                    Total : {{ formating_number($data_all->total()) }}
                    {{ $data_all->appends(request()->query())->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>


@endsection
