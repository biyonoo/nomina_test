@extends('layouts.app')

@section('content')
    
    <div class="container">
        <form method="POST" action="{{ url('data/member') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-sm-center">
                            <div>Data member</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <x-form.text label="first name" for="first_name" name="first_name" value="{{ old('first_name') }}" :error="$errors->first('first_name')" ></x-form.text>  
                                </div>
                                <div class="col-md-6">
                                    <x-form.text label="last name" for="last_name" name="last_name" value="{{ old('last_name') }}" :error="$errors->first('last_name')" ></x-form.text>  
                                </div>
                                <div class="col-md-6">
                                    <x-form.email label="email" for="email" name="email" value="{{ old('email') }}" :error="$errors->first('email')" ></x-form.email>  
                                </div>
                                <div class="col-md-6">
                                    <x-form.password label="password" for="password" name="password" value="{{ old('password') }}" :error="$errors->first('password')" ></x-form.password>  
                                </div>
                                <div class="col-md-6">
                                    <x-form.number label="Age" for="age" name="age" value="{{ old('age') }}" :error="$errors->first('age')" ></x-form.number>  
                                </div>
                                <div class="col-12">
                                    <Label>Hobby</Label>
                                    @forelse($data_hobby as $hobby)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="hobby[]" value="{{ $hobby->id }}" id="{{ $hobby->uuid }}">
                                            <label class="form-check-label" for="{{ $hobby->uuid }}">
                                                {{ $hobby->nama }}
                                            </label>
                                        </div>
                                    @empty
                                    @endforelse
                                </div>  
                                
                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary "><i class="fa fa-save me-2"></i>Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
                    
        </form>
    </div>
@endsection
