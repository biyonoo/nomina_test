@extends('layouts.app')

@section('content')
    
    <div class="container">
        
        <div class="row">
            <div class="col-md-8">
                <form method="POST" action="{{ url('profile') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-sm-center">
                            <div>Data member</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <x-form.text label="first name" for="first_name" name="first_name" value="{{ $data->first_name }}" :error="$errors->first('first_name')" ></x-form.text>
                                </div>
                                <div class="col-md-6">
                                    <x-form.text label="last name" for="last_name" name="last_name" value="{{ $data->last_name }}" :error="$errors->first('last_name')" ></x-form.text>
                                </div>
                                <div class="col-md-6">
                                    <x-form.email label="email" for="email" name="email" value="{{ $data->email }}" :error="$errors->first('email')" ></x-form.email>
                                </div>
                                <div class="col-md-6">
                                    <x-form.number label="Age" for="age" name="age" value="{{ $data->age }}" :error="$errors->first('age')" ></x-form.number>
                                </div>
                                
                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary "><i class="fa fa-save me-2"></i>Simpan</button>
                        </div>
                    </div>
                    
                </form>
            </div>
            <div class="col-md-4">
                <form method="POST" action="{{ url('update_password') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-sm-center">
                            <div>Update Password</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <x-form.password label="password Sekarang" for="current_password" name="current_password" value="" :error="$errors->first('current_password')" ></x-form.password>
                                </div>
                                <div class="col-md-12">
                                    <x-form.password label="password Baru" for="new_password" name="new_password" value="" :error="$errors->first('new_password')" ></x-form.password>
                                </div>
                                <div class="col-md-12">
                                    <x-form.password label="Ulangi password" for="new_confirm_password" name="new_confirm_password" value="" :error="$errors->first('new_confirm_password')" ></x-form.password>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-danger w-100"><i class="fa fa-save me-2"></i>Update Password</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
@endsection
