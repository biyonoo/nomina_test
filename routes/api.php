<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {
    Route::post('/register', [App\Http\Controllers\Api\V1\AuthController::class, 'register']);
    Route::post('/login', [App\Http\Controllers\Api\V1\AuthController::class, 'login']);

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::get('profile', [App\Http\Controllers\Api\V1\ProfileController::class, 'index']);
        Route::post('profile', [App\Http\Controllers\Api\V1\ProfileController::class, 'profile_update']);
        Route::post('update_password', [App\Http\Controllers\Api\V1\ProfileController::class, 'update_password']);


        Route::resource('hobby', App\Http\Controllers\Api\V1\HobbyController::class)->middleware('role:superadmin');
        Route::get('hobby', [App\Http\Controllers\Api\V1\HobbyController::class,'index']);
        Route::get('/hobby/delete/{uuid}', [App\Http\Controllers\Api\V1\HobbyController::class,'destroy'])->middleware('role:superadmin');

        Route::resource('member', App\Http\Controllers\Api\V1\MemberController::class)->middleware('role:superadmin');
        Route::get('/member/delete/{uuid}', [App\Http\Controllers\Api\V1\MemberController::class,'destroy'])->middleware('role:superadmin');
    });
});