<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('profile', [App\Http\Controllers\HomeController::class, 'profile']);
Route::post('/profile', [App\Http\Controllers\HomeController::class, 'profileUpdate']);
Route::post('update_password', [App\Http\Controllers\HomeController::class, 'passwordUpdate']);
Route::post('member', [App\Http\Controllers\HomeController::class, 'updateMember']);
Route::group(['prefix' => 'data', 'middleware' => ['web','auth','role:superadmin']], function () {
    Route::resource('superadmin', App\Http\Controllers\Data\SuperadminController::class)->middleware('role:superadmin|admin');
    Route::get('/superadmin/delete/{uuid}', [App\Http\Controllers\Data\SuperadminController::class,'destroy'])->middleware('role:superadmin|admin');

    Route::resource('member', App\Http\Controllers\Data\MemberController::class);
    Route::get('/member/delete/{uuid}', [App\Http\Controllers\Data\MemberController::class,'destroy']);

    Route::resource('hobby', App\Http\Controllers\Data\HobbyController::class);
    Route::get('/hobby/delete/{uuid}', [App\Http\Controllers\Data\HobbyController::class,'destroy']);
});